FROM dotslashme/alpine-base:3
LABEL image.authors="christian@dotslashme.com"
HEALTHCHECK CMD curl -f http://localhost:9091/transmission/web/ || exit 1

RUN apk add transmission-daemon \
    && rm -rf /var/cache/apk/* \
    && mkdir /opt/transmission \
    && chown -R dockrun: /opt/transmission

VOLUME ["/opt/transmission"]
ENV TRANSMISSION_HOME=/opt/transmission

CMD ["/usr/bin/transmission-daemon", "--foreground", "--config-dir", "/opt/transmission"]
